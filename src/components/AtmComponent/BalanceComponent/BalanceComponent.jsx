
export const BalanceComponent = ({ balance }) => 
  balance && <div> Balance: {balance} </div>
