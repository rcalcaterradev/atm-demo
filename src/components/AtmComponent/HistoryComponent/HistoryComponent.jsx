export const HistoryComponent = ({ history }) => {
 
  return (
    !!history?.length && 
      <div>
        <span>History:</span>
        {history.map((item, index) => <div key={history+index}>{item}</div> )}
      </div>
   
  )
}
