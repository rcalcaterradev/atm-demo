import { useState, useContext } from 'react';
import { UserContext } from '../AtmComponent'

const currentDate = new Date().toString().split(' ').slice(0, 4).join(' ')

const depositHandler = (callback, userAccount, currentValue) => {
  userAccount.balance += currentValue
  userAccount.history.push(`Deposit: ${currentValue}`)
  callback({...userAccount})
}

const withdrawHandler = (callback, userAccount, currentValue) => {
  const dailyWithdrawLimit = 1500
  if (userAccount.balance - currentValue < 0) {
    console.error('Not enough account funds')

  } else if ((userAccount.limitDate === currentDate && userAccount.currentWithdraw + currentValue >= dailyWithdrawLimit) || userAccount.currentWithdraw > dailyWithdrawLimit) {
    console.error("Daily withdraw limit met.")
  } else {
    // balance subtraction
    userAccount.balance -= currentValue
    // withdraw limit balance
    userAccount.currentWithdraw += currentValue
    // history update
    userAccount.history.push(`Withdraw: ${currentValue}`)
    // date check; add or update
    if (userAccount.limitDate !== currentDate) {
      userAccount.limitDate = currentDate
    }
  }
  callback({...userAccount})
}

export const AccountActionButtonComponent = ({ type }) => {
  const [currentValue, updateCurrentValue] = useState(1)

  const { user, setUser } = useContext(UserContext)

  return (
    <div>
      <form onSubmit={(e) => {
        e.preventDefault()
        type === "withdraw" ? 
          withdrawHandler(setUser, user, ~~currentValue) : depositHandler(setUser, user, ~~currentValue)
      }}>
        <label htmlFor={`${type}Input`} />
        <input 
          min={1} 
          type="number" 
          name={`${type}Input`} 
          value={currentValue} 
          onChange={(e) => updateCurrentValue(e.target.value)}
        />
        <button type="submit" className="AccountAction-button">{type}</button>
      </form>
    </div>
  )
}
