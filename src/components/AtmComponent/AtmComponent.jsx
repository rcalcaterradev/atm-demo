import { useState, createContext } from 'react';
import { PinComponent } from './PinComponent/PinComponent'
import { BalanceComponent } from './BalanceComponent/BalanceComponent'
import { HistoryComponent } from './HistoryComponent/HistoryComponent'
import { AccountActionButtonComponent } from './AccountActionButtonComponent/AccountActionButtonComponent'
import { mockUserAccounts } from '../../mocks/mockUserAccounts';

const userDefault = {
  balance: null,
  history: [],
  currentWithdraw: 0    
}

export const UserContext = createContext({
  user: userDefault,
  setUser: () => {},
})

export const AtmComponent = () => {
  const [user, setUser] = useState(userDefault);

  const getUser = pin => {
    const user = mockUserAccounts[pin];
    setUser({...user});
  }

  const { balance, history } = user
  const value = {user, setUser}
  return (
    <UserContext.Provider value={value}>
      <div >
        <PinComponent getUser={getUser} />
        <BalanceComponent balance={balance}/>
        <HistoryComponent history={history}/>
        <div>
          <AccountActionButtonComponent type="deposit" />
          <AccountActionButtonComponent type="withdraw" />
        </div>
      </div>
    </UserContext.Provider>
  )
}