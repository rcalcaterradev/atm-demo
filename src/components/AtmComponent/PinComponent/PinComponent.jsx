import { useState } from 'react'

export const PinComponent = ({ getUser }) => {
  const [currentPin, updateCurrentPin] = useState('')

  const pinHandler = (pin) => {
    getUser(pin)
  }
  
  return (
    <div>
      <form onSubmit={(e) => {
        e.preventDefault()
        pinHandler(currentPin)
      }}>
        <label htmlFor="pinInput">Pin:</label>
        <input name="pinInput" value={currentPin} onChange={(e) => updateCurrentPin(e.target.value)} />
        <button type="submit">search</button>
      </form>
    </div>
  )
}
