import './App.css';

import { AtmComponent } from '../src/components/AtmComponent/AtmComponent'

function App() {
  return (
    <div className="App">
      <div className="App-header">
        <AtmComponent />
      </div>
    </div>
  );
}

export default App;
