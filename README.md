# Starting ATM demo

- If yarn is not installed locally install yarn with `npm install --global yarn`
- Once yarn is installed start the app with `yarn start`

- Theres two mock accounts which can be found at `src/mocks/mockUserAccounts.js` their "pin" numbers are `2321` and `8787`
- Enter either into the pin search and "deposit" or "withdraw" as much as youd like, up to the daily limit of 1500 for withdrawal.